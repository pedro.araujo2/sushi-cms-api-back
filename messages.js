
const MSGS = {
    'CATEGORY404' : 'Categoria não encontrada',
    'CONTENT404' : 'Conteúdo não encontrado',
    'FILE_INVALID_FORMAT' : 'Por favor, envie um arquivo no formato .jpeg ou .png!',    
    'FILE_NOT_SENT' : 'Por favor, envie o arquivo',
    'FILE_UPLOADED' : 'Arquivo enviado com sucesso',
    'GENERIC_ERROR' : 'Erro!',
    'INVALID_PASSWORD' : 'Senha incorreta',
    'INVALID_TOKEN' : 'Token inválido',
    'PRODUCT404' : 'Produto não encontrado',
    'REQUIRED_PASSWORD' : 'Por favor, insira sua senha',
    'USER404' : 'Usuário não encontrado',
    'VALID_EMAIL' : 'Por favor, insira um email válido.',
    'WITHOUT_TOKEN' : 'Token não enviado'    
}

module.exports = MSGS

